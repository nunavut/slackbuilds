//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Text*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"up: ", "uptime -p | cut -d' ' -f2-",	        60,		0},

	{"    mem: ", "free -h | awk '/^Mem/ { print $3\"/\"$2 }'"  ,	2,		0},

	{"    ", "date '+%b, %d %Y %H:%M'",					60,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "\0";
static unsigned int delimLen = 5;
