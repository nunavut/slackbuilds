static const char *background_color = "#20201d";
static const char *border_color = "#30301d";
static const char *font_color = "#a6a28c";
static const char *font_pattern = "DejaVu Sans:size=8";
static unsigned line_spacing = 16;
static unsigned int padding = 10;

static unsigned int width = 200;
static unsigned int border_size = 1;
static unsigned int pos_x = 30;
static unsigned int pos_y = 60;

enum corners { TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT };
enum corners corner = TOP_RIGHT;

static unsigned int duration = 3; /* in seconds */

#define DISMISS_BUTTON Button1
#define ACTION_BUTTON Button3
